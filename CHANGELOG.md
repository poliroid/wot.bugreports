# Changelog

## v10.1.2

* filter out WG error to reduce pressure on server

## v10.1.1

* turn off debug

## v10.1.0

* extended information

## v10.0.0

* first release as a separate project