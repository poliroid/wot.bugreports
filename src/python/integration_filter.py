#
# Imports
#
from .integration_config import OpenWGConfigIntegration

# sentry_sdk
from sentry_sdk.hub import Hub
from sentry_sdk.integrations import Integration
from sentry_sdk.scope import add_global_event_processor


#
# Integration
#

class OpenWGFilterIntegration(Integration):
    identifier = "openwg_filter"

    @staticmethod
    def setup_once():
        @add_global_event_processor
        def processor(event, hint):
            # forward transactions
            if event.get("type") == "transaction":
                return event

            # forward events without hints
            if hint is None:
                return event

            # forward in case of unregistered integration
            integration = Hub.current.get_integration(OpenWGFilterIntegration)
            if integration is None:
                return event

            config = Hub.current.get_integration(OpenWGConfigIntegration)
            if config is None:
                return event

            # process log_record
            log_record = hint.get("log_record", None)
            if log_record is not None:
                message = log_record.getMessage()
                for entry in config.ignore_list:
                    if entry in message:
                        return None

            return event
