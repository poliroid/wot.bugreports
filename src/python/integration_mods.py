#
# Imports
#

# sentry_sdk
from sentry_sdk.hub import Hub
from sentry_sdk.integrations import Integration
from sentry_sdk.scope import add_global_event_processor

# xfw.loader
import xfw_loader.python as loader

# xfw.vfs
import xfw_vfs.python as vfs



#
# Integration
#

class OpenWGModsIntegration(Integration):
    identifier = "openwg_mods"

    def __init__(self):
        self.__installed_mods = dict()

        #xfw
        for key,val in loader.get_mod_ids().items():
            self.__installed_mods[key] = val
        
        #other
        for file in vfs.directory_list_files('scripts/client/gui/mods'):
            file = file.rsplit('.',1)[0]
            if file == '__init__' or file == 'mod_xfw':
                continue
            self.__installed_mods[file] = '0.0.0'


    def get_installed_mods(self):
        return self.__installed_mods


    @staticmethod
    def setup_once():
        @add_global_event_processor
        def processor(event, hint):
            if event.get("type") == "transaction":
                return event

            integration = Hub.current.get_integration(OpenWGModsIntegration)
            if integration is None:
                return event

            event["modules"] = integration.get_installed_mods()
            return event
